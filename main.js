import './style.css';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader'

// Setup
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

const renderer = new THREE.WebGLRenderer({
    canvas: document.querySelector('#bg'),
    alpha: true,
    antialias: true,
});

renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);

camera.position.setZ(28);
camera.position.setY(0);
camera.position.setX(0);

renderer.render(scene, camera);

// Torus


// Lights

const pointLight = new THREE.PointLight(0xffffff);
//pointtLight.position.set(2, 2, 2);

const ambientLight = new THREE.AmbientLight(0xffffff);
scene.add(pointLight, ambientLight);

const controls = new OrbitControls(camera, renderer.domElement);
controls.target.set(0.5, -0.5, 26);
//controls.addEventListener( 'change', render );
//controls.autoRotate = true;

// Background

//const spaceTexture = new THREE.TextureLoader().load('space.jpg');
//scene.background = spaceTexture;

// Avatar

const jeffTexture = new THREE.TextureLoader().load('jeff.png');
const jeff = new THREE.Mesh(new THREE.BoxGeometry(3, 3, 3), new THREE.MeshBasicMaterial({ map: jeffTexture }));

const loader = new FBXLoader();

var object;

loader.load('/table.fbx', function ( object ) {
    object = object;
    object.name = "model";
    window.obj = object;
    object.scale.set(.01, .01, .01);
    object.rotation.set(0, -1, 0);
    object.position.set(1.5, -0.5, 26);
    scene.add( object );
},
function ( xhr ) {
    console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
},
function ( error ) {
    console.log( 'An error happened' );
});

// Scroll Animation

function moveCamera() {
    const t = document.body.getBoundingClientRect().top;
//  scene.rotation.z = t * 0.00550;
    camera.position.x = 0.01550 * t;
}

document.body.onscroll = moveCamera;
moveCamera();

// Animation Loop

const logo_renderer = new THREE.WebGLRenderer({
    canvas: document.querySelector('#logo'),
    alpha: true,
    antialias: true,
});

logo_renderer.setPixelRatio(window.devicePixelRatio);

const logo_scene = new THREE.Scene();
const logo_camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.5, 1000);

const logo_pointLight = new THREE.PointLight(0xffffff);
logo_pointLight.position.set(2, 5, 2);

const logo_ambientLight = new THREE.AmbientLight(0xffffff);
logo_scene.add(logo_pointLight, logo_ambientLight);
logo_camera.position.set(0.0,0,13);

const moonTexture = new THREE.TextureLoader().load('passbild_lebenslauf.png');

var img = new Image();
img.onload = createMeshThenRender;
img.src = 'passbild_lebenslauf.png';
var mesh;

function createMeshThenRender () {
    var imgWidth = 256;
    var imgHeight = 256;
    var mapCanvas = document.createElement( 'canvas' );
    mapCanvas.width = mapCanvas.height = 256;

    // document.body.appendChild( mapCanvas );
    var ctx = mapCanvas.getContext( '2d' );
    ctx.translate( imgWidth / 2, imgHeight / 2 );
    ctx.rotate( 1.6 );
    ctx.translate( -imgWidth / 2, -imgHeight / 2 );
    ctx.drawImage( img, 0, 0, imgWidth, imgHeight );

    var texture = new THREE.Texture( mapCanvas );
    texture.needsUpdate = true;

    mesh = new THREE.Mesh(
        new THREE.CylinderGeometry( 5, 5, 1, 32 ),
        new THREE.MeshBasicMaterial( {
            map : texture
        } )
    );
    mesh.rotation.set(4.8,0.0,0.0);
    logo_scene.add( mesh );
}

//const moon = new THREE.Mesh(
//  new THREE.CylinderGeometry( 5, 5, 1, 32 ),
//  new THREE.MeshStandardMaterial({
//    map: moonTexture,
//  })
//);
//
//moon.position.set(0,0,0);
//moon.rotation.set(5.0,0.0,0.0);
//logo_scene.add(moon);

function animate() {
  requestAnimationFrame(animate);

  mesh.rotation.z += 0.005;
    //scene.rotation.z += 0.005;
   controls.update();
  renderer.render(scene, camera);
  logo_renderer.render(logo_scene, logo_camera);
}

animate();
